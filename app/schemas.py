"""schemas"""
from typing import List

from pydantic import BaseModel


# Request Contract
class ImageMetaDataBase(BaseModel):
    """ImageMetaDataCreate Request Contract."""
    image_name: str
    image_label: str


class ImageMetaDataCreate(ImageMetaDataBase):
    """ImageMetaDataCreate."""
    pass


class ImageMetaData(ImageMetaDataBase):
    """ImageMetaData."""
    id: int
    owner_id: int

    class Config:
        orm_mode = True


class UserBase(BaseModel):
    """Request User Base."""
    email: str


class UserCreate(UserBase):
    """User Create Request."""
    password: str


class User(UserBase):
    """User."""
    id: int
    is_active: bool
    image_data: List[ImageMetaData] = []

    class Config:
        orm_mode = True
