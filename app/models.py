"""models"""
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from .database import Base


# Image MetaData
class ImageMetaData(Base):
    """Image MetaData."""
    __tablename__ = 'ImageMetaData'

    id = Column(Integer, primary_key=True, index=True)
    image_name = Column(String, unique=True, index=True)
    image_url = Column(String, unique=True, index=True)
    image_label = Column(String, unique=True, index=True)
    upload_date = Column(Boolean, default=True)
    owner_id = Column(Integer, ForeignKey('users.id'))

    owner = relationship('User', back_populates='ImageMetaData')


# User
class User(Base):
    """User."""
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    is_active = Column(Boolean, default=True)

    ImageMetaData = relationship('ImageMetaData', back_populates='owner')
