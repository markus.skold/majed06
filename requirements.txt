asyncpg==0.22.0
fastapi==0.75.2
ormar==0.10.5
uvicorn==0.13.4
urllib3==1.22
attrs==19.1.0
aiosqlite==0.17.0
python-multipart
dropbox
pylint
flake8
flake8-import-order
#psycopg[binary]
#python-dotenv==0.19.2
#pylint
#flake8
#flake8-import-order
